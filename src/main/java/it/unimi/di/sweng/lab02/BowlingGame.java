package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	int[] roll = { 0, 0 };
	int match = 0;
	int score = 0;
	int doubnext = 0;

	public void roll(int pins) {
		

		roll[match] = pins;
		if (match == 1) {
			updateScore();
			if ((roll[0] + roll[1]) == 10) {
				doubnext++;
			}
			match = 0;
		} else {
			match++;
			if(roll[0]==10){
				roll(0);
				doubnext++;
			}
			
		}
	}

	private void updateScore() {
		if (doubnext > 0)
			roll[0] *= 2;
		if (doubnext > 1)
			roll[1] *= 2;
		doubnext = 0;
		score = score + roll[0] + roll[1];
	}

	public int score() {
		System.out.println();
		return score;
	}

}
